package controllers

import (
	"net/http"
	"spouki/models"
	"github.com/gorilla/mux"
	"strconv"
)

func UpdateArticle(w http.ResponseWriter, r *http.Request) {
	resp := new(models.ResponseModel)
	article := new(models.Article)
	vars := mux.Vars(r)
	id,err := strconv.Atoi(vars["id"])
	if (err != nil) {
		w.Header().Set("Content-Type", "application/json")
		resp.SetCode(-1)
		resp.SetMessage("Error converting int to string")
		w.Write([]byte(resp.Stringify()))
		return
	}
	article.Id = id
	if (article.Read() == false) {
		w.Header().Set("Content-Type", "application/json")
		resp.SetCode(-1)
		resp.SetMessage("Article not found")
		w.Write([]byte(resp.Stringify()))
		return
	}
	article.Dump(r)
	article.Update()
	resp.Data = article
	resp.SetCode(0)
	resp.SetMessage("Article Modified")
	w.Header().Set("Content-Type", "application/json")
	w.Write([]byte(resp.Stringify()))
}

func DeleteArticle(w http.ResponseWriter, r *http.Request) {
	resp := new(models.ResponseModel)
	article := new(models.Article)
	vars := mux.Vars(r)
	id,err := strconv.Atoi(vars["id"])
	if (err != nil) {
		w.Header().Set("Content-Type", "application/json")
		resp.SetCode(-1)
		resp.SetMessage("Error converting int to string")
		w.Write([]byte(resp.Stringify()))
		return
	}
	article.Id = id
	status := article.Delete()
	if (status == false) {
		w.Header().Set("Content-Type", "application/json")
		resp.SetCode(-1)
		resp.SetMessage("No deletion")
		w.Write([]byte(resp.Stringify()))
		return
	}
	resp.SetCode(0)
	resp.SetMessage("Article deleted")
	w.Header().Set("Content-Type", "application/json")
	w.Write([]byte(resp.Stringify()))
}

func GetArticle(w http.ResponseWriter, r *http.Request) {
	resp := new(models.ResponseModel)
	article := new(models.Article)
	vars := mux.Vars(r)
	id,err := strconv.Atoi(vars["id"])
	if (err != nil) {
		resp.SetCode(-1)
		resp.SetMessage("Error converting id int to string")
		w.Header().Set("Content-Type", "application/json")
		w.Write([]byte(resp.Stringify()))
		return
	}

	article.Id = id
	if (article.Read() == false) {
		resp.SetCode(0)
		resp.SetMessage("Article not found")
		w.Header().Set("Content-Type", "application/json")
		w.Write([]byte(resp.Stringify()))
		return
	}
	resp.SetData(article)
	resp.SetCode(0)
	resp.SetMessage("Article Found")
	w.Header().Set("Content-Type", "application/json")
	w.Write([]byte(resp.Stringify()))
}

func NewArticle(w http.ResponseWriter, r *http.Request) {
	resp := new(models.ResponseModel)
	article := new(models.Article)
	article.Dump(r)
	err := article.Create()
	if (err != nil) {
		resp.SetData(err)
		resp.SetCode(-1)
		resp.SetMessage("Article not created")
		w.Header().Set("Content-Type", "application/json")
		w.Write([]byte(resp.Stringify()))
		return
	}
	resp.SetData(article)
	resp.SetCode(0)
	resp.SetMessage("Article created")
	w.Header().Set("Content-Type", "application/json")
	w.Write([]byte(resp.Stringify()))
}

func GetArticles(w http.ResponseWriter, r *http.Request) {
	resp := new(models.ResponseModel)
	articles := new(models.Articles)
	articles.Read()
	resp.SetData(articles)
	resp.SetCode(0)
	resp.SetMessage("Articles Found")
	w.Header().Set("Content-Type", "application/json")
	w.Write([]byte(resp.Stringify()))
}
