package controllers

import (
	"net/http"
	"fmt"
	"spouki/models"
	"github.com/gorilla/mux"
	"strconv"
)

func UpdateUser(w http.ResponseWriter, r *http.Request) {
	resp := new(models.ResponseModel)
	user := new(models.User)
	vars := mux.Vars(r)
	id,err := strconv.Atoi(vars["id"])
	if (err != nil) {
		w.Header().Set("Content-Type", "application/json")
		resp.SetCode(-1)
		resp.SetMessage("Error converting int to string")
		w.Write([]byte(resp.Stringify()))
		return
	}
	user.Id = id
	if (user.FindById() == false) {
		w.Header().Set("Content-Type", "application/json")
		resp.SetCode(-1)
		resp.SetMessage("User not found")
		w.Write([]byte(resp.Stringify()))
		return
	}
	user.Dump(r)
	user.SaveUpdate()
	resp.Data = user
	resp.SetCode(0)
	resp.SetMessage("User Modified")
	w.Header().Set("Content-Type", "application/json")
	w.Write([]byte(resp.Stringify()))
}

func GetUsers(w http.ResponseWriter, r *http.Request){
	w.Header().Set("Content-Type", "application/json")
	fmt.Printf("Users It seems to work\n")
	w.Write([]byte("{\"hello\": \"world\"}"))
}

func DeleteUserById(w http.ResponseWriter, r *http.Request) {
	resp := new(models.ResponseModel)
	user := new(models.User)
	vars := mux.Vars(r)
	id,err := strconv.Atoi(vars["id"])
	if (err != nil) {
		w.Header().Set("Content-Type", "application/json")
		resp.SetCode(-1)
		resp.SetMessage("Error converting int to string")
		w.Write([]byte(resp.Stringify()))
		return
	}
	user.Id = id
	status := user.Delete()
	if (status == false) {
		w.Header().Set("Content-Type", "application/json")
		resp.SetCode(-1)
		resp.SetMessage("No deletion")
		w.Write([]byte(resp.Stringify()))
		return
	}
	resp.SetCode(0)
	resp.SetMessage("User deleted")
	w.Header().Set("Content-Type", "application/json")
	w.Write([]byte(resp.Stringify()))
}

func GetUserById(w http.ResponseWriter, r *http.Request) {
	resp := new(models.ResponseModel)
	user := new(models.User)
	vars := mux.Vars(r)
	id,err := strconv.Atoi(vars["id"])
	if (err != nil) {
		resp.SetCode(-1)
		resp.SetMessage("Error converting id int to string")
		w.Header().Set("Content-Type", "application/json")
		w.Write([]byte(resp.Stringify()))
		return
	}

	user.Id = id
	if (user.FindById() == false) {
		resp.SetCode(0)
		resp.SetMessage("User not found")
		w.Header().Set("Content-Type", "application/json")
		w.Write([]byte(resp.Stringify()))
		return
	}
	resp.SetData(user)
	resp.SetCode(0)
	resp.SetMessage("User Found")
	w.Header().Set("Content-Type", "application/json")
	w.Write([]byte(resp.Stringify()))
}

func NewUser(w http.ResponseWriter, r *http.Request) {
	resp := new(models.ResponseModel)
	user := new(models.User)
	user.Dump(r)

	if (user.Exists() == true) {
		resp.SetCode(-1)
		resp.SetMessage("User already in DataBase")
		w.Header().Set("Content-Type", "application/json")
		w.Write([]byte(resp.Stringify()))
		return
	}

	err := user.Insert()
	if (err != nil) {
		resp.SetData(err)
		resp.SetCode(-1)
		resp.SetMessage("User not created")
		w.Header().Set("Content-Type", "application/json")
		w.Write([]byte(resp.Stringify()))
		return
	}
	resp.SetData(user)
	resp.SetCode(0)
	resp.SetMessage("User Created")
	w.Header().Set("Content-Type", "application/json")
	w.Write([]byte(resp.Stringify()))
}
