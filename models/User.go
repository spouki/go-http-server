package models

import (
	"time"
	"net/http"
	"github.com/gorilla/schema"
	"fmt"
	"io/ioutil"
	"encoding/json"
	"spouki/utils"
	"spouki/database"
	"github.com/mohae/deepcopy"
	"reflect"
)

type User struct {
	Id int `json:"id""`
	Email string `json:"email"`
	Username string `json:"username"`
	Password string `json:"password"`
	CreationDate time.Time
}

func (u *User) Str() (string, error) {
	convert,err := json.Marshal(u)
	if (err != nil) {
		fmt.Println("Error converting user struct to json text")
		return "",err
	}
	fmt.Println("User : " , convert)
	converted := string(convert)
	fmt.Println("User : " , converted)
	return converted,nil
}

func (u *User) Delete() bool {
	var db = database.GetConnector()
	found := u.FindById()
	if (found == false) {
		fmt.Println("Not found")
		return false
	}
	db.Delete(u)
	fmt.Println("DB ra : " , db.RowsAffected)
	if (db.Error != nil) {
		fmt.Println("No rows affected")
		return false
	}
	return true
}

func (u *User) SaveUpdate() bool {
	var db = database.GetConnector()
	db.Save(u)
	return true
}

func (u *User) FindById() bool {
	var db = database.GetConnector()
	tmpU := deepcopy.Copy(u)
	db.Where("id = ?", u.Id).First(u)
	if (reflect.DeepEqual(tmpU,u) == false) {
		return true
	}
	return false
}

func (u *User) Exists() bool {
	var db = database.GetConnector()
	db.Where("email = ?", u.Email).First(u)
	if (u.Id != 0) {
		fmt.Println("Error user already in db")
		return true
	}
	return false
}

func (u *User) Insert() error {
	var db = database.GetConnector()
	var inserted = db.Create(u) //bool
	fmt.Println("Inserted : " , inserted.RowsAffected)
	if (inserted.RowsAffected < 1) {
		fmt.Println("Error inserting : " , inserted.Error)
		return inserted.Error
	}
	return nil
}

func (u *User) Dump(req *http.Request) bool {
	rp := utils.RequestParse{}
	rp.RequestProcess(req)
	var mime = rp.GetMIMEType(req)
	u.AutoParse(req, mime)
	u.CreationDate = time.Now()
	return true
}


func (j* User) AutoParse(req *http.Request, mime string) {

	if (mime == "application/x-www-form-urlencoded" ||
		(len(mime) > len("multipart/form-data") &&  mime[:len("multipart/form-data")] == "multipart/form-data")) {
		decoder := schema.NewDecoder()
		err := decoder.Decode(j,req.Form)
		if (err != nil) {
			fmt.Printf("Error : %s\n", err)
		}
	} else {
		body,err := ioutil.ReadAll(req.Body)
		if (err != nil) {
			fmt.Printf("Error in parsing body : %s\n", err)
		}
		json.Unmarshal(body, &j)
	}

	fmt.Printf("User : %s\n", j)
}

