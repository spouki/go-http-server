package models

type Collection interface {
	Create() bool
	Read() bool
	Update() bool
	Delete() bool
}
