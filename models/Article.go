package models

import (
	"net/http"
	"time"
	"encoding/json"
	"log"
	"spouki/database"
	"github.com/mohae/deepcopy"
	"reflect"
	"github.com/gorilla/schema"
	"io/ioutil"
	"spouki/utils"
)

type Article struct {
	Id int `json:"id"`
	Title string `json:"title"`
	Content string `json:"content"`
	User int `json:"user`
	Date time.Time `json:"date"`
}


func (u *Article) Create() error {
	var db = database.GetConnector()
	var inserted = db.Create(u) //bool
	log.Println("Inserted : " , inserted.RowsAffected)
	if (inserted.RowsAffected < 1) {
		log.Println("Error inserting : " , inserted.Error)
		return inserted.Error
	}
	return nil
}

func (u *Article) Read() bool {
	var db = database.GetConnector()
	tmpU := deepcopy.Copy(u)
	db.Where("id = ?", u.Id).First(u)
	if (reflect.DeepEqual(tmpU,u) == false) {
		return true
	}
	return false
}

func (u *Article) Update() bool {
	var db = database.GetConnector()
	base := new(Article)
	base.Id = u.Id
	base.Read()
	if (reflect.DeepEqual(base,u) == true) {
		return false
	}
	db.Save(u)
	return true
}

func (u *Article) Delete() bool {
	var db = database.GetConnector()
	found := new(Article)
	found.Id = u.Id
	if (found.Read() == false) {
		log.Println("Not found")
		return false
	}
	db.Delete(u)
	log.Println("DB ra : " , db.RowsAffected)
	if (db.Error != nil) {
		log.Println("No rows affected")
		return false
	}
	return true
}


func (u *Article) Str() (string, error) {
	convert,err := json.Marshal(u)
	if (err != nil) {
		log.Println("Error converting Article struct to json text")
		return "",err
	}
	log.Println("Article : " , convert)
	converted := string(convert)
	log.Println("Article : " , converted)
	return converted,nil
}

func (u *Article) Dump(req *http.Request) bool {
	rp := utils.RequestParse{}
	rp.RequestProcess(req)
	var mime = rp.GetMIMEType(req)
	u.AutoParse(req, mime)
	u.Date = time.Now()
	return true
}


func (j* Article) AutoParse(req *http.Request, mime string) {

	if (mime == "application/x-www-form-urlencoded" ||
		(len(mime) > len("multipart/form-data") &&  mime[:len("multipart/form-data")] == "multipart/form-data")) {
		decoder := schema.NewDecoder()
		err := decoder.Decode(j,req.Form)
		if (err != nil) {
			log.Printf("Error : %s\n", err)
		}
	} else {
		body,err := ioutil.ReadAll(req.Body)
		if (err != nil) {
			log.Printf("Error in parsing body : %s\n", err)
		}
		json.Unmarshal(body, &j)
	}

	log.Printf("Article : %s\n", j)
}

