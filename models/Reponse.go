package models

import (
	"encoding/json"
	"fmt"
)

type ResponseModel struct {
	Code int `json:"Code"`
	Message string `json:"Message"`
	Data interface{} `json:"Data"`
}

func (r *ResponseModel) SetCode(c int) {
	r.Code = c
}

func (r *ResponseModel) SetMessage(m string) {
	r.Message = m
}

func (r *ResponseModel) SetData(v interface{}) {
	r.Data = v
}

func (r *ResponseModel) Stringify() string {
	resp,err := json.Marshal(r)
	if (err != nil) {
		fmt.Println("Can't marshal " , r)
	}
	fmt.Println("Response : " , string(resp[:]))
	return string(resp[:])
}