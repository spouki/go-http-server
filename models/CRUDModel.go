package models

import "net/http"

type CRUDModel interface {
	AutoParse(*http.Request, string)
	Create() error
	Read() bool
	Update() bool
	Delete() bool
	Dump(*http.Request)
	Str() string
}
