package models

import (
	"spouki/database"
	"github.com/mohae/deepcopy"
	"reflect"
)

type Articles struct {
	Articles []Article `json:"Articles"`
}

func (A *Articles) Read() bool {
	var db = database.GetConnector()
	tmpA := deepcopy.Copy(A)
	db.Find(&A.Articles)
	
	return !reflect.DeepEqual(tmpA,A)
}

func (A *Articles) Create() bool {
	return false
}

func (A *Articles) Delete() bool {
	return false
}

func (A *Articles) Update() bool {
	return false
}
