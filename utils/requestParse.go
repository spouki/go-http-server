package utils

import (
	"net/http"
	"fmt"
)

type RequestParse struct {
}

func (r *RequestParse) RequestProcess(req* http.Request) bool {
	req.ParseMultipartForm(2048) // FIXME : We need to handle dynamic size, 1stly for avoiding using unnecessary memory space and 2ndly for handling large size of files in requests.
	return true
}

func (r *RequestParse) DisplayMIMEType(req* http.Request) {
	fmt.Printf("Request is of type %s\n",
		req.Header.Get("Content-Type"))
}

func (r *RequestParse) GetMIMEType(req* http.Request) (string) {
	return req.Header.Get("Content-Type")
}

func (r* RequestParse) DisplayContent(req* http.Request) {
	for key, value := range req.Form {
		fmt.Printf("%s = %s\n", key,value)
	}
}
