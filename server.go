package main

import (
	"github.com/gorilla/mux"
	articles "spouki/handlers/articles"
	users "spouki/handlers/users"
	"net/http"
	"log"
	"time"
	"os"
	"os/signal"
	"flag"
	"context"
	"github.com/rs/cors"
	_ "spouki/database"
	"spouki/database"
)

func MiddleWare(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Println("middleware ", r.URL)

		token := r.Header.Get("Authorization")
		contenttype := r.Header.Get("Content-Type")
		log.Println("Token : ", token)
		log.Println("Content-Type : ", contenttype)
		if (token == "DEBUGTOKEN") {
			h.ServeHTTP(w,r)
		} else {
			http.Error(w, "Forbidden", http.StatusForbidden)
		}
	})
}

func main() {

	var wait time.Duration
	flag.DurationVar(&wait, "graceful-timeout", time.Second * 15, "the duration for which the server gracefully wait for existing connections to finish - e.g. 15s or 1m")
	flag.Parse()
	r := mux.NewRouter()
	articles.ArticlesHandler(r)
	users.UsersHandler(r)
	r.Use(MiddleWare)
	CorsHandler := cors.New(cors.Options{
		AllowedOrigins: []string{"*"},
		AllowCredentials: true,
		AllowedMethods: []string{"GET","POST","DELETE",
			"PATCH","OPTIONS"},
	})
	handler := CorsHandler.Handler(r)	
	srv := &http.Server{
		Handler:      handler,
		Addr:         "127.0.0.1:8080",
		// Good practice: enforce timeouts for servers you create!
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}
	
	go func() {
		database.Init()
		log.Println("Launching server")
		if err := srv.ListenAndServe(); err != nil {
			log.Println(err)
			database.Exit()
		}
	}()

	c := make(chan os.Signal, 1)
	// We'll accept graceful shutdowns when quit via SIGINT (Ctrl+C)
	// SIGKILL, SIGQUIT or SIGTERM (Ctrl+/) will not be caught.
	signal.Notify(c, os.Interrupt)

	// Block until we receive our signal.
	<-c

	// Create a deadline to wait for.
	ctx, cancel := context.WithTimeout(context.Background(), wait)
	defer cancel()
	// Doesn't block if no connections, but will otherwise wait
	// until the timeout deadline.
	srv.Shutdown(ctx)
	// Optionally, you could run srv.Shutdown in a goroutine and block on
	// <-ctx.Done() if your application should wait for other services
	// to finalize based on context cancellation.
	log.Println("shutting down")
	os.Exit(0)
}
