package auth

import (
	"golang.org/x/net/context"
	firebase "firebase.google.com/go"
	"google.golang.org/api/option"
)


func init() {
	opt := option.WithCredentialsFile("path/to/key.json")
	app, err := firebase.NewApp(context.Background(), nil, opt)
}
