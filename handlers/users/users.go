package handlers

import (
	"github.com/gorilla/mux"
	controller "spouki/controllers/users"
)

func UsersHandler(r* mux.Router) {
	s := r.PathPrefix("/users").Subrouter()
	s.HandleFunc("/",controller.GetUsers).Methods("GET","OPTIONS")
	s.HandleFunc("/", controller.NewUser).Methods("POST","OPTIONS")
	s.HandleFunc("/{id}",controller.GetUserById).Methods("GET","OPTIONS")
	s.HandleFunc("/{id}",controller.DeleteUserById).Methods("DELETE","OPTIONS")
	s.HandleFunc("/{id}",controller.UpdateUser).Methods("PATCH","OPTIONS")
}
