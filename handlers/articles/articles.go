package handlers

import (
	"github.com/gorilla/mux"
	controller "spouki/controllers/articles"
)

func ArticlesHandler(r* mux.Router) {
	s := r.PathPrefix("/articles").Subrouter()
	s.HandleFunc("/",controller.GetArticles).Methods("GET","OPTIONS")
	s.HandleFunc("/",controller.NewArticle).Methods("POST","OPTIONS")
	s.HandleFunc("/{id}", controller.GetArticle).Methods("GET","OPTIONS")
	s.HandleFunc("/{id}", controller.DeleteArticle).Methods("DELETE","OPTIONS")
	s.HandleFunc("/{id}", controller.UpdateArticle).Methods("PATCH","OPTIONS")
}
